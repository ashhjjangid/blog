<?php

function json_output($data, $die = true) {

	header('Content-Type: application/json');
	echo json_encode($data);
	if ($die) {
		die;
	}
}

function show_limited_text($string, $len = 10) {
	$string = strip_tags($string);
	if (strlen($string) > $len) {
		$string = mb_substr($string, 0, $len - 3) . "...";
	}

	return $string;
}

function encrypt_password($password) {
	$encrypt_pass = md5(md5($password));
	return $encrypt_pass;
}

if (!function_exists('print_arr')) {

	function print_arr($data_arr) {
		echo '<pre>';
		print_r($data_arr);
		echo '<pre>';
	}
}

if (!function_exists('LookForDataRecords')) {
	
	function LookForDataRecords($data) {

		if (!$data) {
            show_404();
        }
        return true;
	}
}


?>