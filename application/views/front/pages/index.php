

  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto ajax_data_body">
        <div class="post-preview">
          <a href="post.html">
            <h2 class="post-title">
              Man must explore, and this is exploration at its greatest
            </h2>
            <h3 class="post-subtitle">
              Problems look mighty small from 150 miles up
            </h3>
          </a>
          <p class="post-meta">Posted by
            <a href="#"  data-poster="<?php echo (isset($value->author) && $value->author)?$value->author:''; ?>" class="author">Start Bootstrap</a>
            on September 24, 2019</p>
        </div>
        <hr>
        <div class="post-preview">
          <a href="post.html">
            <h2 class="post-title">
              I believe every human has a finite number of heartbeats. I don't intend to waste any of mine.
            </h2>
          </a>
          <p class="post-meta">Posted by
            <a href="#" class="author">Start Bootstrap</a>
            on September 18, 2019</p>
        </div>
        <hr>
        <div class="post-preview">
          <a href="post.html">
            <h2 class="post-title">
              Science has not yet mastered prophecy
            </h2>
            <h3 class="post-subtitle">
              We predict too much for the next year and yet far too little for the next ten.
            </h3>
          </a>
          <p class="post-meta">Posted by
            <a href="#">Start Bootstrap</a>
            on August 24, 2019</p>
        </div>
        <hr>
        <div class="post-preview">
          <a href="post.html">
            <h2 class="post-title">
              Failure is not an option
            </h2>
            <h3 class="post-subtitle">
              Many say exploration is part of our destiny, but it’s actually our duty to future generations.
            </h3>
          </a>
          <p class="post-meta">Posted by
            <a href="#">Start Bootstrap</a>
            on July 8, 2019</p>
        </div>
        <hr>
        <!-- Pager -->
      </div>
        <div class="clearfix">
          <!-- <a class="btn btn-primary float-right" href="#">Older Posts &rarr;</a> -->
        </div>
    </div>
    <div class="paging_area pagination">
        
    </div>
  </div>

  <script type="text/javascript">
    function searchdatabyauthor() {

      var site_url = '<?php echo site_url(); ?>';
      var blog_poster = $('.author').attr('data-poster');
      var add_date = $('.author').attr('data-posted-date');
      var page_limit = "10";

      var surl = site_url+'home/get_ajax_list_of_blogs?page_limit='+page_limit+'&blog_poster='+blog_poster;
      getAjaxData(surl);
    }

    function getAjaxData(surl) {
      $.getJSON(surl, function(data) {
        if (data.success) {
          $('.ajax_data_body').html(data.html);
          $('.paging_area').html(data.paging);
        }
      });
    }

     $(document).ready(function() {
        searchdatabyauthor();
        $(document).on('click', '.pagination a', function(e) {
            e.preventDefault();
            if($(this).attr('href'))
            {
                getAjaxData($(this).attr('href'));
            }
            return false;
        })
        $('.author').keydown(function(event) {
        // enter has keyCode = 13, change it if you want to use another button
            if (event.keyCode == 13) {
                // event.preventDefault();
                searchRecords();
            }
        });
    });
  </script>