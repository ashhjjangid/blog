 

<div class="post-preview">
    <?php foreach ($records as $key => $value) { ?>
        
    <a href="<?php echo base_url('home/view_story/'.$value->id); ?>">
        <h2 class="post-title">
            <?php echo $value->blog_title; ?>
        </h2>
    </a>
    <p class="post-meta">Posted by
        <a href="#" onclick="searchdatabyauthor()" data-poster="<?php echo (isset($value->author) && $value->author)?$value->author:''; ?>" class="author"><?php echo (isset($value->author) && $value->author)?$value->author:''; ?></a>
        on <?php echo date('M d, Y', strtotime($value->add_date)) ?>
    </p>
    <hr>
    <?php } ?>
</div>