<!DOCTYPE html>
<html lang="en">
<head>
	<title>Blog</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<script src="<?php echo $this->config->item('front_assets'); ?>vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="<?php echo $this->config->item('front_assets'); ?>js/main.js"></script>
	<script src="<?php echo $this->config->item('front_assets'); ?>scripts/common.js"></script>
	<script src="<?php echo $this->config->item('front_assets'); ?>scripts/jquery.form.js"></script>
	<script src="<?php echo base_url() ?>/assets/admin/black_layout/bootstrap-toastr/toastr.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('admin_assets'); ?>/bootstrap-toastr/toastr.min.css"/>
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('front_assets'); ?>vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('front_assets'); ?>fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('front_assets'); ?>fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('front_assets'); ?>vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('front_assets'); ?>vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('front_assets'); ?>vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('front_assets'); ?>vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('front_assets'); ?>vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('front_assets'); ?>css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('front_assets'); ?>css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-t-85 p-b-20">
				<form method="post" id="login_form" action="<?php echo current_url(); ?>" class="login100-form validate-form ajax_form" enctype="multipart/form-data">
					<span class="login100-form-title p-b-70">
						Welcome
					</span>
					<!-- <span class="login100-form-avatar">
						<img src="images/avatar-01.jpg" alt="AVATAR">
					</span> -->

					<div class="wrap-input100 validate-input m-t-85 m-b-35" data-validate = "Enter Email">
						<input class="input100" type="text" name="email">
						<span class="focus-input100" data-placeholder="Email"></span>
					</div>

					<div class="wrap-input100 validate-input m-b-50" data-validate="Enter password">
						<input class="input100" type="password" name="password">
						<span class="focus-input100" data-placeholder="Password"></span>
					</div>

					<div class="container-login100-form-btn">
						<button type="submit" class="login100-form-btn">
							Login
						</button>
					</div>

					<!-- <ul class="login-more p-t-190">
						<li class="m-b-8">
							<span class="txt1">
								Forgot
							</span>

							<a href="#" class="txt2">
								Username / Password?
							</a>
						</li>

						<li>
							<span class="txt1">
								Don’t have an account?
							</span>

							<a href="#" class="txt2">
								Sign up
							</a>
						</li>
					</ul> -->
				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
<!--===============================================================================================-->
	<script src="<?php echo $this->config->item('front_assets'); ?>vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo $this->config->item('front_assets'); ?>vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo $this->config->item('front_assets'); ?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo $this->config->item('front_assets'); ?>vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo $this->config->item('front_assets'); ?>vendor/daterangepicker/moment.min.js"></script>
	<script src="<?php echo $this->config->item('front_assets'); ?>vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo $this->config->item('front_assets'); ?>vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->

</body>
</html>