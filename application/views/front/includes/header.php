<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Blog</title>

  <script src="<?php echo base_url() ?>assets/admin/black_layout/scripts/jquery.min.js"></script>
  <script src="<?php echo base_url() ?>assets/admin/black_layout/scripts/common.js"></script>
  <!-- Bootstrap core CSS -->
  <link href="<?php echo $this->config->item('front_assets'); ?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="<?php echo $this->config->item('front_assets'); ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="<?php echo $this->config->item('front_assets'); ?>css/clean-blog.min.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand" href="<?php echo base_url('home'); ?>">Ashu Blogs</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('home'); ?>">Home</a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link" href="about.html">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="post.html">Sample Post</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="contact.html">Contact</a>
          </li> -->
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Header -->
  <?php $blog_image = (isset($record->blog_image) && $record->blog_image)?$record->blog_image:'';
        $blog_id = (isset($record->id) && $record->id)?$record->id:'';
        $blog_title = (isset($record->blog_title) && $record->blog_title)?$record->blog_title:'';
        $blog_author = (isset($record->author) && $record->author)?$record->author:'';
        $blog_date = (isset($record->add_date) && $record->add_date)?$record->add_date:'';


  ?>
  <header class="masthead" style="background-image: url('<?php echo base_url('/assets/images/').$blog_image; ?>')">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <div class="post-heading">
                    <h1><a href="<?php echo base_url('home/view_story/').$blog_id; ?>"><?php echo $blog_title; ?></a></h1>
                    <span class="meta">Posted by
                        <a href="#" data-poster="<?php echo (isset($blog_author) && $blog_author)?$blog_author:''; ?>"><?php echo $blog_author; ?></a>
                        on <?php echo date('M d, Y', strtotime($blog_date)); ?>
                    </span>
                </div>
            </div>
        </div>
    </div>
</header>
 