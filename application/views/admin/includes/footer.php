        <div class="footer-copyright-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer-copy-right">
                            <p>Copyright © 2020 <a href="https://www.linkedin.com/in/ashu-k-jangid/" target="_blank">Ashok Jangid...</a> All rights reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
    <!-- jquery
		============================================ -->
    <script src="<?php echo $this->config->item('admin_assets'); ?>js/plugins.js"></script>
	<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script> -->
	
    <!-- <script src="<?php echo $this->config->item('admin_assets'); ?>js/vendor/jquery-1.12.4.min.js"></script> -->
    <script src="https://code.jquery.com/jquery-3.0.0.js"></script>
    <!-- <script src="https://code.jquery.com/jquery-migrate-3.3.1.js"></script> -->
    <!-- bootstrap JS
		============================================ -->
    <script src="<?php echo $this->config->item('admin_assets'); ?>js/bootstrap.bundle.min.js"></script>
    <!-- <script src="<?php echo $this->config->item('admin_assets'); ?>js/Popper.js"></script> -->
    <!-- wow JS
		============================================ -->
    <script src="<?php echo $this->config->item('admin_assets'); ?>js/wow.min.js"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="<?php echo $this->config->item('admin_assets'); ?>js/jquery-price-slider.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="<?php echo $this->config->item('admin_assets'); ?>js/jquery.meanmenu.js"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="<?php echo $this->config->item('admin_assets'); ?>js/owl.carousel.min.js"></script>
    <!-- sticky JS
		============================================ -->
    <script src="<?php echo $this->config->item('admin_assets'); ?>js/jquery.sticky.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="<?php echo $this->config->item('admin_assets'); ?>js/jquery.scrollUp.min.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="<?php echo $this->config->item('admin_assets'); ?>js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="<?php echo $this->config->item('admin_assets'); ?>js/scrollbar/mCustomScrollbar-active.js"></script>
    <!-- metisMenu JS
		============================================ -->
    <script src="<?php echo $this->config->item('admin_assets'); ?>js/metisMenu/metisMenu.min.js"></script>
    <script src="<?php echo $this->config->item('admin_assets'); ?>js/metisMenu/metisMenu-active.js"></script>
    <!-- sparkline JS
		============================================ -->
    <script src="<?php echo $this->config->item('admin_assets'); ?>js/sparkline/jquery.sparkline.min.js"></script>
    <script src="<?php echo $this->config->item('admin_assets'); ?>js/sparkline/jquery.charts-sparkline.js"></script>
    <!-- calendar JS
		============================================ -->
    <script src="<?php echo $this->config->item('admin_assets'); ?>js/calendar/moment.min.js"></script>
    <script src="<?php echo $this->config->item('admin_assets'); ?>js/calendar/fullcalendar.min.js"></script>
    <script src="<?php echo $this->config->item('admin_assets'); ?>js/calendar/fullcalendar-active.js"></script>
	<!-- float JS
		============================================ -->
    <script src="<?php echo $this->config->item('admin_assets'); ?>js/flot/jquery.flot.js"></script>
    <script src="<?php echo $this->config->item('admin_assets'); ?>js/flot/jquery.flot.resize.js"></script>
    <script src="<?php echo $this->config->item('admin_assets'); ?>js/flot/curvedLines.js"></script>
    <script src="<?php echo $this->config->item('admin_assets'); ?>js/flot/flot-active.js"></script>
    <!-- plugins JS
		============================================ -->
	<script src="<?php echo base_url() ?>assets/admin/black_layout/scripts/jquery.form.js"></script>
    


    <!-- main JS
		============================================ -->
    <script src="<?php echo $this->config->item('admin_assets'); ?>js/main.js"></script>

</html>