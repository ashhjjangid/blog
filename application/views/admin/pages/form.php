
            <div class="breadcome-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="breadcome-list">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="breadcomb-wp">
											<div class="breadcomb-icon">
												<i class="icon nalika-home"></i>
											</div>
											<div class="breadcomb-ctn">
												<h2 class="page-title">
                                                    <?php if (isset($record->id) && $record->id) { ?>
                                                        
                                                        Edit Blog Details
                                                     <?php } else { ?>

                                                        Add Blog Details
                                                     <?php } ?>
                                                </h2>
											</div>
										</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Single pro tab start-->
        <div class="single-product-tab-area mg-b-30">
            <!-- Single pro tab review Start-->
            <div class="single-pro-review-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="review-tab-pro-inner">
                                <ul id="myTab3" class="tab-review-design">
                                    <li class="active"><a href="#description"><i class="icon nalika-edit" aria-hidden="true"></i><?php if (isset($record->id) && $record->id) { ?>
                                            Edit
                                        <?php } else { ?> 
                                            Add
                                        <?php } ?> 
                                    </a></li>                                    
                                </ul>
                                <div id="myTabContent" class="tab-content custom-product-edit">
                                	<form method="post" class="ajax_form" id="blog_details" action="<?php echo current_url(); ?>" enctype="multipart/form-data">
                                    <div class="product-tab-list tab-pane fade active in" id="description">
                                        
                                        <div class="input-group mg-b-pro-edt">

                                            <span class="input-group-addon"><i class="icon nalika-menu-task" aria-hidden="true"></i></span>
                                            <input type="text" name="blog_title" class="form-control" placeholder="Blog Title" value="<?php echo (isset($record->blog_title) && $record->blog_title)?$record->blog_title:''; ?>">
                                            <!-- <input type="text" name="description" class="form-control" placeholder="Product Description"> -->
                                        </div>

                                        <div class="input-group mg-b-pro-edt">

                                            <span class="input-group-addon"><i class="icon nalika-user" aria-hidden="true"></i></span>
                                            <input type="text" name="author" class="form-control" placeholder="Blog Author" value="<?php echo (isset($record->author) && $record->author)?$record->author:''; ?>">
                                        </div>

                                        <div class="input-group mg-b-pro-edt">

                                            <span class="input-group-addon"><i class="icon nalika-favorites-button" aria-hidden="true"></i></span>
                                            <textarea name="description" class="form-control" placeholder="Blog Description"><?php echo (isset($record->blog_description) && $record->blog_description)?$record->blog_description:''; ?></textarea>
                                            <!-- <input type="text" name="description" class="form-control" placeholder="Product Description"> -->
                                        </div>
                                        <div class="input-group">
                                            <div class="text-muted">
                                                <p>Status: </p>
                                            </div>
                                            <div class="form-radio">
                                                <div class="radio radiofill">
                                                    <label>
                                                            <input type="radio" name="status" value="Active" <?php echo (isset($record->status) && $record->status == 'Active')?'checked':''; ?>><i class="helper"></i>Active
                                                        </label>
                                                </div>

                                                <div class="radio radiofill">
                                                    <label>
                                                            <input type="radio" name="status" value="Inactive" <?php echo (isset($record->status) && $record->status == 'Inactive')?'checked':''; ?>><i class="helper"></i>Inactive
                                                        </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="review-content-section">
                                                    <div class="row">
                                                        
                                                        
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="product-edt-pix-wrap">
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon">Images</span>
                                                                            <input type="file" class="form-control" name="photos" placeholder="Add Photos">
                                                                        </div>
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-center custom-pro-edt-ds">
                                            <!-- <div class="col-lg-6"> -->
                                                <div class="pro-edt-img">
                                                    <?php if (isset($record->id) && $record->id) { ?>
                                                        
                                                        <?php if (isset($images) && $images) { ?>
                                                                
                                                            <img style="width:250px;height:150px; padding-right: 10px" src="<?php echo base_url().'assets/images/'.$images ?>" alt="" />
                                                        
                                                        <?php } else { ?>
                                                            <img style="width:200px;height:150px;" src="<?php echo $this->config->item('assets'); ?>no-img/no-img-available.jpg" alt="" />
                                                        <?php } ?>
                                                    <?php } else { ?>

                                                    <?php } ?>
                                                </div>
                                            <!-- </div> -->
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="text-center custom-pro-edt-ds">
                                                    <button type="submit" class="btn btn-ctl-bt waves-effect waves-light m-r-10">Save</button>
                                                    <!-- <button type="submit" class="btn green">Submit
                                                    </button> -->
                                                    <a href="<?php echo base_url('admin/cars'); ?>" class="btn btn-ctl-bt waves-effect waves-light">Discard</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </form>                                 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>