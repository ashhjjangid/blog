<?php foreach ($records as $key => $value) { ?>
    
<tr>
    <td>
        <?php if($value->blog_image) { ?>
            <img src="<?php echo base_url('/assets/images/').$value->blog_image; ?>" alt="" />
        <?php } else { ?>
            <img src="<?php echo base_url(); ?>assets/no-img/no-img-available.jpg" alt="" />
        <?php } ?>
    </td>
    <td><?php echo show_limited_text($value->blog_title); ?></td>
    <td>
        <button class="<?php echo (isset($value->status) && $value->status == 'Active')?'pd-setting':'ds-setting'; ?>"><?php echo $value->status; ?></button>
    </td>
    <td><?php echo show_limited_text($value->blog_description); ?></td>
    <td><?php echo date('d-m-Y H:i:s', strtotime($value->add_date)); ?></td>
    <!-- <td>Out Of Stock</td> -->
    <!-- <td>$15</td> -->
    <td>
        <a href="<?php echo base_url('admin/blog/update/'. $value->id)?>" data-toggle="tooltip" title="Edit" class="pd-setting-ed"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
        <a href="<?php echo base_url('admin/blog/delete/'. $value->id)?>" data-toggle="tooltip" title="Delete" class="pd-setting-ed remove-data"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
        <a href="<?php echo base_url('admin/blog/view/'. $value->id)?>" data-toggle="tooltip" title="View" class="pd-setting-ed remove-data"><i class="icon nalika-folder" aria-hidden="true"></i></a>
    </td>
</tr>
<?php } ?>