
            <div class="breadcome-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="breadcome-list">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="breadcomb-wp">
											<div class="breadcomb-icon">
												<i class="icon nalika-home"></i>
											</div>
											<div class="breadcomb-ctn">
												<h2 class="page-title">Blog Details</h2>
											</div>
										</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Single pro tab start-->
        <div class="single-product-tab-area mg-t-0 mg-b-30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
						<div class="single-product-pr">
							<div class="row">
								<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
									<div id="myTabContent1" class="tab-content">
										<div class="product-tab-list tab-pane fade active in" id="single-tab1">
											<?php if($record->blog_image) { ?>
                                                <img src="<?php echo base_url('/assets/images/').$record->blog_image; ?>" alt="" />
                                            <?php } else { ?>
                                                <img src="<?php echo base_url(); ?>assets/no-img/no-img-available.jpg" alt="" />
                                            <?php } ?>
										</div>
										
									</div>
									
								</div>
								<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
									<div class="single-product-details res-pro-tb">
										<h1><?php echo ($record->blog_title)?$record->blog_title:''; ?></h1>
											<div class="single-pro-button">
												<div class="pro-button">
													<a href="#"><?php echo ($record->status == 'Active')?'Active':'Inactive'; ?></a>
												</div>												
											</div>
											<div class="clear"></div>
											<div class="single-social-area">
												<h3>share this on</h3>
												<a href="#"><i class="fa fa-facebook"></i></a>
												<a href="#"><i class="fa fa-google-plus"></i></a>
												<a href="#"><i class="fa fa-feed"></i></a>
												<a href="#"><i class="fa fa-twitter"></i></a>
												<a href="#"><i class="fa fa-linkedin"></i></a>
											</div>
										</div>
										<div class="single-pro-cn">
											<h3>Description</h3>
											<p><?php echo ($record->blog_description)?$record->blog_description:''; ?></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
