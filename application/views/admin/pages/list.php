            <div class="breadcome-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="breadcome-list">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="breadcomb-wp">
											<div class="breadcomb-icon">
												<i class="icon nalika-home"></i>
											</div>
											<div class="breadcomb-ctn">
												<h2 class="page-title">Blog List</h2>
											</div>
										</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="product-status mg-b-30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-status-wrap">
                            <h4>List of Blogs</h4>
                            <div class="breadcome-heading">
                                <form role="search" class="">
                                    <input type="text" name="search_keyword" placeholder="Search..." class="form-control" id="search-keyword" onkeyup="searchDataRecords()" />
                                </form>
                            </div>
                            <div class="add-product">
                                <a href="<?php echo base_url('admin/blog/add'); ?>">Add New Blog</a>
                            </div>
                            <table>
                                <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>Blog Title</th>
                                        <th>Status</th>
                                        <th>Description</th>
                                        <th>Add Date</th>
                                        <th>Options</th>
                                    </tr>
                                </thead>
                                <tbody class="ajax_data_body">
                                    <tr>
                                        <td>No Record Found</td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                            <div class="custom-pagination">
                                <div class="paging_area pagination">
                                    
                                </div>
								
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<script type="text/javascript">
    
    function searchDataRecords() {
        var site_url = '<?php echo base_url(); ?>';
        var keyword = $('#search-keyword').val();
        //alert(keyword);
        var page_limit = "10";

        var surl = site_url+'admin/blog/get_ajax_list_of_blogs?keyword='+keyword+'&page_limit='+page_limit;
        getAjaxSearchData(surl);
    }

    function getAjaxSearchData(surl) {
        $.getJSON(surl, function(data) {

            if (data.success) {
                $('.ajax_data_body').html(data.html);
                $('.paging_area').html(data.paging);
            }
        });
    }



    $(document).ready(function() {
        searchDataRecords();
        $(document).on('click', '.pagination a', function(e) {
            e.preventDefault();
            if($(this).attr('href'))
            {
                getAjaxSearchData($(this).attr('href'));
            }
            return false;
        })
        $('#keyword').keydown(function(event) {
        // enter has keyCode = 13, change it if you want to use another button
            if (event.keyCode == 13) {
                // event.preventDefault();
                searchRecords();
            }
        });
    });

    function carRecordPhotos() {

    }
</script>
