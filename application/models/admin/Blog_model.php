<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**Car_users_model
 * 
 */
class Blog_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->users = 'tbl_users';
		$this->blogs = 'tbl_blogs';
		$this->blogs_image = 'tbl_car_images';
	}

	function countTotalRecordsOfBlogs($searchData) {
		$this->db->select($this->blogs.'.*');
		if (isset($searchData) && $searchData['keyword']) {
			$this->db->group_start();
			$this->db->or_like($this->blogs.'.blog_title', $searchData['keyword']);
			$this->db->or_like($this->blogs.'.add_date', $searchData['keyword']);
			$this->db->group_end();
		}
		$query = $this->db->get($this->blogs);
		return $query->num_rows();
	}

	function getTotalRecordsOfBlogs($searchData) {
		$this->db->select($this->blogs.'.*');
		if (isset($searchData) && $searchData['keyword']) {
			$this->db->group_start();
			$this->db->or_like($this->blogs.'.blog_title', $searchData['keyword']);
			$this->db->or_like($this->blogs.'.add_date', $searchData['keyword']);
			$this->db->group_end();
		}

		if (isset($searchData) && $searchData['limit']) {
			$this->db->limit($searchData['limit'], $searchData['search_index']);
		}
		$query = $this->db->get($this->blogs);
		//print_arr($this->db->last_query());
		return $query->result();
	}

	public function get_all_users()
	{
		$query = $this->db->get($this->users);
		return $query->result();
	}

	function AddNewBlog($data) {
		$this->db->insert('tbl_blogs', $data);
		return $this->db->insert_id();
	}

	function updateBlogDetails($id, $data) {
		$this->db->where('id', $id);
		$this->db->update($this->blogs, $data);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}

	function getRecordById($id) {
		$this->db->where('id', $id);
		$query = $this->db->get($this->blogs);
		return $query->row();
	}

	function DeleteBlog($blog_id) {
		$this->db->where('id', $blog_id);
		$this->db->delete($this->blogs);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}
}