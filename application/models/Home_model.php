<?php

class Home_model extends CI_Model
{
	function __construct() {
		parent::__construct();
		$this->blogs = 'tbl_blogs';
	}
	
	function countlatestblogsrecords($search_data) {

		if (isset($search_data) && $search_data['keyword']) {
			$this->db->group_start();
			$this->db->or_like($this->blogs.'.author', $search_data['keyword']);
			$this->db->or_like($this->blogs.'.add_date', $search_data['keyword']);
			$this->db->group_end();
		}
		$this->db->where('status', 'Active');
		$query = $this->db->get($this->blogs);
		return $query->num_rows();
	}

	function getlatestblogrecords($search_data) {
		if (isset($search_data) && $search_data['keyword']) {
			$this->db->group_start();
			$this->db->or_like($this->blogs.'.author', $search_data['keyword']);
			$this->db->or_like($this->blogs.'.add_date', $search_data['keyword']);
			$this->db->group_end();
		}

		if (isset($search_data) && $search_data['page_limit']) {
			$this->db->limit($search_data['page_limit'], $search_data['search_index']);
		}

		$this->db->where('status', 'Active');
		$query = $this->db->get($this->blogs);
		return $query->result();
	}

	function getRecordById($id) {
		$this->db->where('id', $id);
		$query = $this->db->get($this->blogs);
		return $query->row();
	}

	function getLastBlog() {
		$this->db->where('status', 'Active');
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->blogs);
		return $query->row();
	}

	function get_user_for_login($email, $pass) {
		$this->db->where('email', $email);
		$this->db->where('password', $pass);
		$query = $this->db->get('tbl_users');
		return $query->row();
	}

	function get_user_details($id) {
		$this->db->where('id', $id);
		$query = $this->db->get('tbl_users');
		return $query->row();
	}
}