<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct() 
	{
		parent::__construct();
		$this->load->model('home_model');
	}

	function index() {

		$last_record = $this->home_model->getLastBlog();
		$output['record'] = $last_record;

		$this->load->view('front/includes/header', $output);
		$this->load->view('front/pages/index');
		$this->load->view('front/includes/footer');
	}

	function get_ajax_list_of_blogs() {
		$success = true;
		$html = false;
		$load_prev_link = false;
		$load_next_link = false;
		$blog_poster = $this->input->get('author');
		$blog_date = $this->input->get('add_date');
		$page_no = $this->input->get('page_no') ? $this->input->get('page_no') : 1;
		$page_limit = $this->input->get('page_limit');

		$page_no_index = ($page_no - 1) * $page_limit;

		$queries = '';
		if ($blog_poster) {
			$queries = $queries.'&posted_by='.$blog_poster;
		}
		if ($blog_date) {
			$queries = $queries.'&date='.$blog_date;
		}
		if ($page_limit) {
			if ($blog_poster || $blog_date) {
				
				$queries = $queries.'&page_limit='.$page_limit;
			} else {

				$queries = $queries.'page_limit='.$page_limit;
			}
		}

		$search_data['search_index'] = $page_no_index;
		$search_data['page_limit'] = $page_limit;
		$search_data['search_index'] = $page_no_index;
		$search_data['keyword'] = $blog_poster;
		$config['base_url'] = base_url('home/get_ajax_list_of_blogs?'.$queries);
        $total_rows = $this->home_model->countlatestblogsrecords($search_data);
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $page_limit;
		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$paging = $this->pagination->create_links();
		$records = $this->home_model->getlatestblogrecords($search_data);
		//print_arr($records); die;
        $output['records'] = $records;
        $html = $this->load->view('front/pages/ajax_listing', $output, true);
        $data['success'] = true;
        $data['html'] = $html;
        $data['paging'] = $paging;

        json_output($data);
	}

	function view_story($id) {

		$record = $this->home_model->getRecordById($id);

		$output['record'] = $record;

		$this->load->view('front/includes/header', $output);
		$this->load->view('front/pages/blog_view');
		$this->load->view('front/includes/footer');
	}
}
