<?php defined('BASEPATH') or exit('No direct script access allowed');

/**Cars Controller
 * 
 */
class Blog extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/blog_model', 'blog_model');
	}

	function index() {

		if (!$this->session->userdata('user_id')) {
			redirect('register/login');
		}
		
		$this->load->view('admin/includes/header');
		$this->load->view('admin/pages/list');
		$this->load->view('admin/includes/footer');
	}

	function get_ajax_list_of_blogs() {

		$success = true;
		$html = false;
		$load_prev_link = false;
		$load_next_link = false;
		$keyword = $this->input->get('keyword');
		$page_no = $this->input->get('page_no') ? $this->input->get('page_no') : 1;
		$page_limit = $this->input->get('page_limit');
		//print_arr($page_limit); die;
		$page_no_index = ($page_no - 1) * $page_limit;
		$sQuery = '';

		if($keyword)
        {
            $sQuery = $sQuery.'&keyword='.$keyword;
        }
        if($page_limit)
        {
            $sQuery = $sQuery.'&page_limit='.$page_limit;       
        }
        $search_data['search_index'] = $page_no_index;
        $search_data['limit'] = $page_limit;
        $search_data['keyword'] = $keyword;
        //print_arr($search_data); die;
        $config['base_url'] = base_url('admin/blog/get_ajax_list_of_blogs?'.$sQuery);
        $total_rows = $this->blog_model->countTotalRecordsOfBlogs($search_data);
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $page_limit;
		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$paging = $this->pagination->create_links();
		$records = $this->blog_model->getTotalRecordsOfBlogs($search_data);
        //print_arr($records); die;
        $output['records'] = $records;
        $html = $this->load->view('admin/pages/ajax_listing', $output, true);
        $data['success'] = true;
        $data['html'] = $html;
        $data['paging'] = $paging;

        json_output($data);
	}

	function add() {

		if (!$this->session->userdata('user_id')) {
			redirect('register/login');
		}
		$output['blog_title'] = '';
		$output['blog_description'] = '';
		$output['status'] = 'Active';
		$output['record'] = '';
			//print_arr($_FILES);
		if ($this->input->post()) {
			//print_arr($_POST); die;

			$success = true;
			$message = '';
		
			$this->form_validation->set_rules('blog_title', 'Blog Title', 'trim|required');
			$this->form_validation->set_rules('description', 'Description', 'trim|required');
			

			if ($this->form_validation->run()) {

				//print_arr($image_count); die;

				if (isset($_FILES['photos']['tmp_name']) && $_FILES['photos']['tmp_name']) {
					
					$directory = './assets/images/';
					@mkdir($directory, 0777);
					@chmod($directory, 0777);
					$config['upload_path'] = $directory;
					$config['allowed_types'] = 'gif|png|jpeg|jpg';
					$config['encrypt_name'] = TRUE;
					$this->load->library('upload', $config);
					$this->upload->initialize($config);

					if ($this->upload->do_upload('photos')) {
						
						$image_data = $this->upload->data();
						$file_name = $image_data['file_name'];
						$input['blog_image'] = $file_name;
						$success = true;
					} else {
						$message = $this->upload->display_errors();
						$success = false;
					}						
					
				}

				if ($success) {
				//print_arr($_POST); die;
					
					$input['blog_title'] = $this->input->post('blog_title');
					$input['blog_description'] = $this->input->post('description');
					$input['author'] = $this->input->post('author');
					$input['status'] = $this->input->post('status');			
					$input['add_date'] = date('Y-m-d H:i:s');

					if (!empty($file_name) && $file_name) {
						
						$input['blog_image'] = $file_name;
					}
					$insert_id = $this->blog_model->AddNewBlog($input);

					if ($insert_id) {
						

						$message = 'New Blog Added Successfully';
						$success = true;
						$data['redirectURL'] = base_url('admin/dashboard');
					} else {
						$message = 'Something went Wrong! Please Try Again Later...';
						$success = false;
					}					
				}
				
			} else {
				$message = validation_errors();
				$success = false;
			}
			$data['success'] = $success;
			$data['message'] = $message;
			json_output($data);
			/*if ($this->input->is_ajax_request()) {
				//die('here');
                json_output($data);
            }*/
		}

		$this->load->view('admin/includes/header', $output);
		$this->load->view('admin/pages/form');
		$this->load->view('admin/includes/footer');
	}

	function update($id) {

		if (!$this->session->userdata('user_id')) {
			redirect('register/login');
		}
			
		$record = $this->blog_model->getRecordById($id);
		//xprint_arr($record); die;
		LookForDataRecords($record);
		if ($record->blog_image) {
			$output['images'] = $record->blog_image;
		} else {
			$output['images'] = '';
		}
		$output['record'] = $record;

		if ($this->input->post()) {
			//print_arr($_POST); die;

			$success = true;
			$message = '';
		
			$this->form_validation->set_rules('blog_title', 'Title', 'trim|required');
			$this->form_validation->set_rules('description', 'Description', 'trim|required');

			if ($this->form_validation->run()) {

				//print_arr($image_count); die;

				if (isset($_FILES['photos']['tmp_name']) && $_FILES['photos']['tmp_name']) {
					
					$directory = './assets/images/';
					@mkdir($directory, 0777);
					@chmod($directory, 0777);
					$config['upload_path'] = $directory;
					$config['allowed_types'] = 'gif|png|jpeg|jpg';
					$config['encrypt_name'] = TRUE;
					$this->load->library('upload', $config);
					$this->upload->initialize($config);

					if ($this->upload->do_upload('photos')) {
						
						$image_data = $this->upload->data();
						$file_name = $image_data['file_name'];
						$input['blog_image'] = $file_name;
					} else {
						$this->upload->display_errors();
						$success = false;
					}						
					
				}

				if ($success) {
					
					$input['blog_title'] = $this->input->post('blog_title');
					$input['blog_description'] = $this->input->post('description');		
					$input['author'] = $this->input->post('author');		
					$input['status'] = $this->input->post('status');			
					$input['add_date'] = date('Y-m-d H:i:s');

					if (!empty($file_name) && $file_name) {
						
						$input['blog_image'] = $file_name;
					}

					$response = $this->blog_model->updateBlogDetails($id, $input);

					if ($response) {

						$message = 'Blog Details Updated Successfully';
						$success = true;
						$data['redirectURL'] = base_url('admin/blog');
					} else {
						$message = 'Something went Wrong! Please Try Again Later...';
						$success = false;
					}					
				}
				
			} else {
				$message = validation_errors();
				$success = false;
			}
			$data['success'] = $success;
			$data['message'] = $message;
			json_output($data);
			/*if ($this->input->is_ajax_request()) {
				//die('here');
                json_output($data);
            }*/
		}

		$this->load->view('admin/includes/header', $output);
		$this->load->view('admin/pages/form');
		$this->load->view('admin/includes/footer');
	}

	function view($id) {

		$record = $this->blog_model->getRecordById($id);
		$output['record'] = $record;

		$this->load->view('admin/includes/header', $output);
		$this->load->view('admin/pages/view');
		$this->load->view('admin/includes/footer');
	}

	function delete($id) {

		$this->blog_model->DeleteBlog($id);

		redirect('admin/blog');
	}
}