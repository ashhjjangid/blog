<?php defined('BASEPATH') or exit('No direct script access allowed');

/**Dashboard Controller
 * 
 */
class Dashboard extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/blog_model', 'blog_model');

	}

	function index() {

		if (!$this->session->userdata('user_id')) {
			redirect('register/login');
		}
		
		$this->load->view('admin/includes/header');
		$this->load->view('admin/pages/list');
		$this->load->view('admin/includes/footer');
	}

	function get_ajax_list_of_blogs() {

		$success = true;
		$html = false;
		$load_prev_link = false;
		$load_next_link = false;
		$keyword = $this->input->get('keyword');
		$page_no = $this->input->get('page_no') ? $this->input->get('page_no') : 1;
		$page_limit = $this->input->get('page_limit');
		//print_arr($page_limit); die;
		$page_no_index = ($page_no - 1) * $page_limit;
		$sQuery = '';

		if($keyword)
        {
            $sQuery = $sQuery.'&keyword='.$keyword;
        }
        if($page_limit)
        {
            $sQuery = $sQuery.'&page_limit='.$page_limit;       
        }
        $search_data['search_index'] = $page_no_index;
        $search_data['limit'] = $page_limit;
        $search_data['keyword'] = $keyword;
        //print_arr($search_data); die;
        $config['base_url'] = base_url('admin/dashboard/get_ajax_list_of_blogs?'.$sQuery);
        $total_rows = $this->blog_model->countTotalRecordsOfBlogs($search_data);
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $page_limit;
		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$paging = $this->pagination->create_links();
		$records = $this->blog_model->getTotalRecordsOfBlogs($search_data);
        //print_arr($records); die;
        $output['records'] = $records;
        $html = $this->load->view('admin/pages/ajax_listing', $output, true);
        $data['success'] = true;
        $data['html'] = $html;
        $data['paging'] = $paging;

        json_output($data);
	}
}