<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	function __construct() 
	{
		parent::__construct();
		$this->load->model('home_model', 'user');
	}

	public function login()
	{

		if ($this->session->userdata('user_id')) {
			redirect('admin/dashboard');
		}

		if ($this->input->post()) {

			$success = true;
			$message = false;
			
			$this->form_validation->set_rules('email', 'Email', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');

			if ($this->form_validation->run()) {
				
				$email = $this->input->post('email');
				$password = $this->input->post('password');
				$encrypt_pass = encrypt_password($password);
				
				$valid_user = $this->user->get_user_for_login($email, $encrypt_pass);
				if ($valid_user) {
					$user = $this->user->get_user_details($valid_user->id);
					//print_r($valid_user); die('here');
					$message = "Welcome ". $valid_user->first_name;
					$this->session->set_userdata(array('user_id' => $user->id, 'name' => $user->first_name, 'email' => $user->email));
					$data['redirectURL'] = base_url('admin/dashboard');	
				} else {
					$success = false;
					$message = 'Incorrect Username or Password';
				}
				//print_r($input); die;
			} else {
				$success = false;
				$message = validation_errors();
			}
			$data['success'] = $success;
			$data['message'] = $message;
			//json_output($data);
			if ($this->input->is_ajax_request()) {
				//die('here');
                json_output($data);
            }
		}
		$this->load->view('front/pages/login');
	}

	function signup() {

		if ($this->session->userdata('user_id')) {
			redirect('admin/dashboard');
		}

		$success = true;
		$message = '';
		if ($this->input->post()) {
			
			$this->form_validation->set_rules('name', 'Name', 'required|trim');
			$this->form_validation->set_rules('username', 'Username', 'required|trim|is_unique[tbl_users.username]');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[tbl_users.email]');
			$this->form_validation->set_rules('mobile', 'Mobile Number', 'required|trim');

			if ($this->form_validation->run()) {
				$input = array();

				$name = $this->input->post('name');
				$username = $this->input->post('username');
				$password = $this->input->post('password');
				$encrypt_pass = encrypt_password($password);
				$email = $this->input->post('email');
				$mobile = $this->input->post('mobile');

				$input['name'] = $name; 
				$input['username'] = $username;
				$input['email']	= $email;
				$input['password'] = $encrypt_pass;
				$input['mobile_number'] = $mobile;
				//print_r($input); die;
				$insert_id = $this->user->add_user($input);

				if ($insert_id) {
					$success = true;
					$message = "You are successfully registered";
					$this->session->set_userdata(array('user_id' => $insert_id, 'name' => $name, 'email'  => $email));
					$data['redirectURL'] = base_url('admin/dashboard');
				} else {
					$success = false;
					$message = "Something went wrong! Try Again...";
				}
			} else {
				$success = false;
				$message = validation_errors();
			}
			$data['success'] = $success;
			$data['message'] = $message;
			//json_output($data);
			if ($this->input->is_ajax_request()) {
				//die('here');
                json_output($data);
            }
		}
		$this->load->view('front/includes/header');
		$this->load->view('front/register/register_page');
		$this->load->view('front/includes/footer');
	}
}
